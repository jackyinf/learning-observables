import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

i18n
  .use(LanguageDetector)
  .init({
    fallbackLng: 'en',

    // have a common namespace used around the full app
    ns: ['common'],
    defaultNS: 'common',

    debug: true,

    interpolation: {
      escapeValue: false // not needed for react!!
    },

    resources: {
      en: {
        transl: require('./en/transl.json')
      },
      ee: {
        transl: require('./ee/transl.json')
      }
    }
  });


export default i18n;
