import React, { Component, PropTypes } from 'react'
import { browserHistory, Router } from 'react-router'
import { Provider } from 'react-redux'
import { I18nextProvider } from 'react-i18next';
import i18n from '../locales/i18n'; // initialized i18next instance

// import './../locales/en/transl.json';
// import './../locales/ee/transl.json';

class AppContainer extends Component {
  static propTypes = {
    routes : PropTypes.object.isRequired,
    store  : PropTypes.object.isRequired
  };

  shouldComponentUpdate () {
    return false
  }

  render () {
    const { routes, store } = this.props;

    return (
      <I18nextProvider i18n={ i18n }>
        <Provider store={store}>
          <div style={{ height: '100%' }}>
            <Router history={browserHistory} children={routes} />
          </div>
        </Provider>
      </I18nextProvider>
    )
  }
}

export default AppContainer
