/**
 * Created by zekar on 10/9/2016.
 */

import Rx from 'rxjs/Rx';

/*
  BehaviorSubject - replays one, only before completion
 */
var subject = new Rx.BehaviorSubject(0);

var observerA = {
  next: (x) => console.log(`A next ${x}`),
  error: (err) => console.log(`A error ${err}`),
  complete: () => console.log(`A done`)
};

subject.subscribe(observerA);
console.log('observerA subscribed');

var observerB = {
  next: (x) => console.log(`B next ${x}`),
  error: (err) => console.log(`B error ${err}`),
  complete: () => console.log(`B done`)
};

subject.next(1);
subject.next(2);
subject.next(3);

setTimeout(() => {
  subject.subscribe(observerB);
  console.log('observerB subscribed');
}, 2000);
