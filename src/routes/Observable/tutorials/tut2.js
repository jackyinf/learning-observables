/**
 * Created by zekar on 10/9/2016.
 */

import Rx from 'rxjs/Rx';

var observable = Rx.Observable.interval(1000).take(5);

// Hybrid - observable and observer - that means it is subject
/*
var bridgetObserver = {
  next: function (x) {
    this.observers.forEach(o => o.next(x));
  },
  error: function (err) {
    this.observers.forEach(o => o.error(err));
  },
  complete: function () {
    this.observers.forEach(o => o.complete());
  },
  observers: [],
  subscribe: function (observer) {
    this.observers.push(observer);
  }
};
*/
// Alternatively, we use subject for this purpose
var subject = new Rx.Subject();

var observerA = {
  next: (x) => console.log(`A next ${x}`),
  error: (err) => console.log(`A error ${err}`),
  complete: () => console.log(`A done`)
};

// Variant 1 - Bridge
/*
observable.subscribe(bridgetObserver);  // create an execution
bridgetObserver.subscribe(observerA);
*/
// Variant 2 - Subject
observable.subscribe(subject);
subject.subscribe(observerA);

var observerB = {
  next: (x) => console.log(`B next ${x}`),
  error: (err) => console.log(`B error ${err}`),
  complete: () => console.log(`B done`)
};

setTimeout(() => {
  // bridgetObserver.subscribe(observerB);
  subject.subscribe(observerB);
}, 2000);
