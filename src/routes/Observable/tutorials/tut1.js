/**
 * Created by zekar on 10/9/2016.
 */

import Rx from 'rxjs/Rx';

var observable = Rx.Observable.interval(1000).take(5);

var observerA = {
  next: (x) => console.log(`A next ${x}`),
  error: (err) => console.log(`A error ${err}`),
  done: () => console.log(`A done`)
};

observable.subscribe(observerA);  // create an execution

var observerB = {
  next: (x) => console.log(`B next ${x}`),
  error: (err) => console.log(`B error ${err}`),
  done: () => console.log(`B done`)
};

setTimeout(() => {
  observable.subscribe(observerB);
}, 2000);
