/**
 * Created by zekar on 10/9/2016.
 */

import Rx from 'rxjs/Rx';

function subjectFactory() {
  return new Rx.Subject();
}

const shared = Rx.Observable.interval(1000).take(6)
  .do(x => console.log(`source ${x}`))
  .map(x => Math.random());

var result = shared
  .multicast(subjectFactory, function selector(shared) {
    // selector function acts as a sandbox

    var sharedDelayed = shared.delay(500);
    var merged = shared.merge(sharedDelayed);
    return merged;
  });

// when we subscribe to result, we subscribe to multicast,
// which subscribes to shared source
result.subscribe(x => console.log(x));
