/**
 * Created by zekar on 10/9/2016.
 */

import Rx from 'rxjs/Rx';

var shared = Rx.Observable.interval(1000)
  .do(x => console.log(`source ${x}`))
  .share(); // or .publish().refCount();

// publish(): multicast + Subject (shortcut to ".multicast(new Rx.Subject());")
// publishReplay(100): multicast + ReplaySubject
// publishBehaviour('initial'): multicast + BehaviourSubject
// publishLast(): multicast + AsyncSubject

// shortcut share - publish().refCount();

var observerA = {
  next: (x) => console.log(`A next ${x}`),
  error: (err) => console.log(`A error ${err}`),
  complete: () => console.log(`A done`)
};

var subA = shared.subscribe(observerA);  // start, cuz nr of observers 0 -> 1

var observerB = {
  next: (x) => console.log(`B next ${x}`),
  error: (err) => console.log(`B error ${err}`),
  complete: () => console.log(`B done`)
};

var subB;
setTimeout(() => {
  subB = shared.subscribe(observerB);  // nr of observers 1 -> 2
}, 2000);

setTimeout(function () {
  subA.unsubscribe(); // 2 -> 1
  console.log('ubsubscirbed A');
}, 5000);

setTimeout(function () {
  subB.unsubscribe(); // 1 -> 0 (stop)
  console.log('ubsubscirbed B');
}, 7000);
