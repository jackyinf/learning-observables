/**
 * Created by zekar on 10/9/2016.
 */

import Rx from 'rxjs/Rx';

var connectableObservable = Rx.Observable.interval(1000)
  .do(x => console.log(`source ${x}`))
  .multicast(new Rx.Subject());

// refCount
var autoConnectedObservable = connectableObservable.refCount();
// if number of observers changes from 0 to 1, it will automatically connects (subscribes)
// if 1 -> 0, it automatically unsubscribes

var observerA = {
  next: (x) => console.log(`A next ${x}`),
  error: (err) => console.log(`A error ${err}`),
  complete: () => console.log(`A done`)
};

var subA = autoConnectedObservable.subscribe(observerA);  // start, cuz nr of observers 0 -> 1

var observerB = {
  next: (x) => console.log(`B next ${x}`),
  error: (err) => console.log(`B error ${err}`),
  complete: () => console.log(`B done`)
};

var subB;
setTimeout(() => {
  subB = autoConnectedObservable.subscribe(observerB);  // nr of observers 1 -> 2
}, 2000);

setTimeout(function () {
  subA.unsubscribe(); // 2 -> 1
  console.log('ubsubscirbed A');
}, 5000);

setTimeout(function () {
  subB.unsubscribe(); // 1 -> 0 (stop)
  console.log('ubsubscirbed B');
}, 7000);
