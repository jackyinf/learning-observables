/**
 * Created by zekar on 10/9/2016.
 */

import Rx from 'rxjs/Rx';

var connectableObservable = Rx.Observable.interval(1000)
  .do(x => console.log(`source ${x}`))
  .multicast(new Rx.Subject());

var observerA = {
  next: (x) => console.log(`A next ${x}`),
  error: (err) => console.log(`A error ${err}`),
  complete: () => console.log(`A done`)
};

var sub = connectableObservable.connect();  // start
var subA = connectableObservable.subscribe(observerA);

var observerB = {
  next: (x) => console.log(`B next ${x}`),
  error: (err) => console.log(`B error ${err}`),
  complete: () => console.log(`B done`)
};

var subB;
setTimeout(() => {
  subB = connectableObservable.subscribe(observerB);
}, 2000);

setTimeout(function () {

  /*
    Variant A: unsubscribe form each. Bad: leak persists
   */
  // subA.unsubscribe();
  // subB.unsubscribe();
  // console.log('ubsubscirbed both');

  /*
    Variant B: unsubscribe shared. Good.
   */
  sub.unsubscribe();
  console.log('ubsubscirbed shared execution');
}, 5000);
