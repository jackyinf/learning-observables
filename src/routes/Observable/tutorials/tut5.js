/**
 * Created by zekar on 10/9/2016.
 */

import Rx from 'rxjs/Rx';

/*
 ReplaySubject replays many, before or after completion
 */

/*
const bufferSize = 2; // how many last values we will see
// const bufferSize = Number.POSITIVE_INFINITY;
const windowSize = 250;  // how long in time (ms) replay subject keeps in store values
var subject = new Rx.ReplaySubject(bufferSize, windowSize);
*/
var subject = new Rx.ReplaySubject(100);

var observerA = {
  next: (x) => console.log(`A next ${x}`),
  error: (err) => console.log(`A error ${err}`),
  complete: () => console.log(`A done`)
};

subject.subscribe(observerA);
console.log('observerA subscribed');

var observerB = {
  next: (x) => console.log(`B next ${x}`),
  error: (err) => console.log(`B error ${err}`),
  complete: () => console.log(`B done`)
};

setTimeout(() => subject.next(1), 100);
setTimeout(() => subject.next(2), 200);
setTimeout(() => subject.next(3), 300);

setTimeout(() => subject.complete(3), 350);

setTimeout(() => {
  subject.subscribe(observerB);
  console.log('observerB subscribed');
}, 400);
