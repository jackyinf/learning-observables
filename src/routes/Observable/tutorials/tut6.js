/**
 * Created by zekar on 10/9/2016.
 */

import Rx from 'rxjs/Rx';

/*
  AsyncSubject - replays one, only if completed.
  Used for heavy computations. Rarely used.
 */
var subject = new Rx.AsyncSubject();

var observerA = {
  next: (x) => console.log(`A next ${x}`),
  error: (err) => console.log(`A error ${err}`),
  complete: () => console.log(`A done`)
};

subject.subscribe(observerA);
console.log('observerA subscribed');

var observerB = {
  next: (x) => console.log(`B next ${x}`),
  error: (err) => console.log(`B error ${err}`),
  complete: () => console.log(`B done`)
};

setTimeout(() => subject.next(1), 100);
setTimeout(() => subject.next(2), 200);
setTimeout(() => subject.next(3), 300);

setTimeout(() => subject.complete(3), 350);

setTimeout(() => {
  subject.subscribe(observerB);
  console.log('observerB subscribed');
}, 400);
