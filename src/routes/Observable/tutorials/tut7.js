/**
 * Created by zekar on 10/9/2016.
 */

import Rx from 'rxjs/Rx';

/*
 Connectable Observable - observable, backed by a subject.
 We can hide subject with it.
 */
const source = Rx.Observable.interval(1000).take(5);
var connectableObservable = source.multicast(new Rx.Subject());

var observerA = {
  next: (x) => console.log(`A next ${x}`),
  error: (err) => console.log(`A error ${err}`),
  complete: () => console.log(`A done`)
};

connectableObservable.connect();  // execute observable (source), backed by subject
connectableObservable.subscribe(observerA); // subject.subscribe(observerA)

var observerB = {
  next: (x) => console.log(`B next ${x}`),
  error: (err) => console.log(`B error ${err}`),
  complete: () => console.log(`B done`)
};

setTimeout(() => {
  connectableObservable.subscribe(observerB);
}, 2000);
