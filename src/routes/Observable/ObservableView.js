/**
 * Created by zekar on 10/9/2016.
 */

import React from 'react';
// import './tutorials/tut1';
// import './tutorials/tut2';
import Elem from './tutorials/tut3';
// import './tutorials/tut5';
// import './tutorials/tut6';
// import './tutorials/tut7';
// import './tutorials/tut8';
// import './tutorials/tut9';
// import './tutorials/tut10';
// import './tutorials/tut11';
import './tutorials/tut12';

class ObservableView extends React.Component {
  render() {
    return (<div>
      Observable View
      <Elem name="Jev" />
    </div>);
  }
}

export default ObservableView;

/**
 * Why use Rx subjects.
 *
 * 1. If doing some side effect and if I don't want to do it for each
 * observer, then I use subject
 *
 * 2. ReplaySubject and BehaviourSubject to see previous data
 *
 * 3. Subject is the only object which is list of observers.
 * It's like adding a listener.
 *
 * 4. We can use Subject as an event emitter. Don't overabuse.
 * Use observers for this purpose.
 *
 * Takeaway: Subjects are necessary. You can do wrong stuff with it
 * pretty easily, so use with multicast to hide them. Then you get
 * benefit of subjects without dangers.
 */
