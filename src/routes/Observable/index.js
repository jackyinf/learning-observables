/**
 * Created by zekar on 10/9/2016.
 */

export default (store) => ({
  path: 'observable',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const ObservableView = require('./ObservableView').default;
      cb(null, ObservableView);
    }, 'observable');
  }
});
