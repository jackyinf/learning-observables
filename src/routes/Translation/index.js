/**
 * Created by jevgenir on 10/12/2016.
 */

export default (route) => ({
  path: 'translation',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const TranslationView = require('./TranslationView').default;
      cb(null, TranslationView);
    });
  }
})
