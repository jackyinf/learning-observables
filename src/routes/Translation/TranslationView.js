/**
 * Created by jevgenir on 10/12/2016.
 */

import React from 'react';
import { translate } from 'react-i18next';
import i18n from 'i18next';

@translate(['transl'], { wait: true })
class TranslationView extends React.Component {
  handleLanguageChange = (e, lang) => {
    i18n.changeLanguage(lang);
  };

  render() {
    const {t} = this.props;

    return (<div>
      Translation - {t('transl:link1')}
      <button onClick={e => this.handleLanguageChange(e, 'en')}>Set estonian</button>
      <button onClick={e => this.handleLanguageChange(e, 'ee')}>Set english</button>
    </div>)
  }
}

export default TranslationView;
